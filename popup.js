function init() {
    return function () {
        fetch('./manifest.json')
            .then(response => response.json())
            .then(json => {
                document.getElementById('version').innerText = json.version;
            });
        $("button").click(function() {
            var email = document.getElementById('email').value;
            var country = $(this).data('country');
            var isBusiness = $(this).data('business');
            var addressData = {
                NL: {
                    zip: '6468ES',
                    houseno: '9',
                    address: 'Mercuriusstraat',
                    billing_city: 'Heerlen',
                    billing_company: isBusiness == '1' ? 'Allekabels BV' : '',
                    billing_phone: isBusiness == '1' ? '0455717001' : '',
                    tax_number: isBusiness == '1' ? 'NL821473013B01' : '',
                    kvk_number: isBusiness == '1' ? '14123564' : '',
                },
                BE: {
                    zip: '8200',
                    houseno: '10',
                    address: 'Leiselsestraat',
                    billing_city: 'Brugge',
                    billing_company: isBusiness == '1' ? 'Allekabels BV' : '',
                    billing_phone: isBusiness == '1' ? '0455717001' : '',
                    tax_number: isBusiness == '1' ? 'BE0760854538' : '',
                    kvk_number: isBusiness == '1' ? '' : '',
                },
                DE: {
                    zip: '10589',
                    houseno: '1',
                    address: 'Gaußstr',
                    billing_city: 'Berlin',
                    billing_company: isBusiness == '1' ? 'Allekabels BV' : '',
                    billing_phone: isBusiness == '1' ? '0455717001' : '',
                    tax_number: isBusiness == '1' ? 'DE295295432' : '',
                    kvk_number: isBusiness == '1' ? 'HRB157289B' : '',
                },
            };
            var address = addressData[country];
            chrome.tabs.query({active: true, currentWindow: true}, tabs => {
                chrome.tabs.executeScript(
                    tabs[0].id,
                    {
                        code: 'if(document.getElementById("chktype").getElementsByTagName("option").length>0) { document.getElementById("chktype").getElementsByTagName("option")['+(isBusiness+1)+'].selected=true;}'
                            + 'if(document.getElementById("chktype").tagName == "input") { document.getElementById("chktype").value = '+ (isBusiness ? 2 : 1)+';}'
                            + 'document.getElementById("billing_fname").value="Tester T";'
                            + 'document.getElementById("billing_lname").value="Testerson";'
                            + 'document.getElementById("billing_country").value="' + country + '";'
                            + 'document.getElementById("billing_zip1").value="' + address.zip + '";'
                            + 'document.getElementById("billing_homeno").value="' + address.houseno + '";'
                            + 'document.getElementById("billing_address").value="' + address.address + '";'
                            + 'document.getElementById("billing_city").value="' + address.billing_city + '";'
                            + 'document.getElementById("billing_company").value="' + address.billing_company + '";'
                            + 'document.getElementById("tax_number").value="' + address.tax_number + '";'
                            + 'document.getElementById("kvk_number").value="' + address.kvk_number + '";'
                            + 'document.getElementById("billing_phone").value="' + address.billing_phone + '";'
                            + 'document.getElementById("billing_email").value="' + email + '";'
                            + 'window.scrollTo(0,document.body.scrollHeight);'
                    }
                );
            });
         });
    }
}
document.addEventListener('DOMContentLoaded', init(), false);

